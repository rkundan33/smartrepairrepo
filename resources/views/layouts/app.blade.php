<!DOCTYPE html>
<html dir="ltr" lang="en-US">


<!-- Mirrored from www.repairdevice-demo2.moonwebsolutions.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 17 Apr 2022 09:40:51 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <!-- Document Title -->
    <meta name="keywords" content="Get your Device Repaired Today!" />
    <meta name="description" content="Get your Device Repaired Today!" />
    <title>Get your Device Repaired Today! fdsafdsa</title>
    <meta property="og:title" content="FB OG Title" />
    <meta property="og:description" content="FB OG Description" />
    <meta property="og:image" content="{{ asset('') }}media/images/og_facebook_img.jpg" />
    <meta property="og:url" content="index.html" />
    <meta property="twitter:title" content="Twitter OG Title" />
    <meta property="twitter:description" content="Twitter OG Description" />
    <meta property="twitter:image" content="{{ asset('') }}media/images/og_twitter_img.jpg" />
    <meta name="twitter:card" content="summary_large_image"> <!-- Stylesheets -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link
        href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i"
        rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('') }}assets/css/style.css" type="text/css" />
    <link rel="stylesheet" href="{{ asset('') }}assets/css/theme_color.css" type="text/css" />
    <link rel="stylesheet" href="{{ asset('') }}assets/css/icon-font.min.css" type="text/css" />
    <link rel="stylesheet" href="{{ asset('') }}assets/css/fonts/font.css" type="text/css" />

    @yield('style_content')
    <style>
        .textt {
            font-size: 26px !important;
            margin-top: 0px !important;
            margin-bottom: 0px !important;
        }

    </style>
</head>

<body class="stretched no-transition home-page">
    <!-- Document Wrapper -->
    <div id="wrapper" class="clearfix">

        <!-- Top Bar -->
        <div id="top-bar" class="header_topbar">
            <div class="container clearfix">
                <div class="col_half nobottommargin store_detail">
                    <p class="nobottommargin information_detail"><span class="incomning-outgoining-call"><strong><i
                                    class="icon-call"></i></strong>&nbsp; <a
                                href="tel:6201018042">6201018042</a></span> </p>
                </div>
                <div class="col_half col_last fright nobottommargin">
                    <!-- Top Links -->
                    <div class="rightside_information_link top-links">
                        <ul>
                            <li>
                                <a class="myaccount " href="#">Quick Repair</a>
                            </li>
                            <li>
                                <a class="myaccount " href="#">Locations&nbsp;<i class="fa fa-map-marker"
                                        aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a class="myaccount " href="#">Track Order&nbsp;<i class="fa fa-compass"
                                        aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="icon-user-alt"></i> Member area</a>
                            </li>
                        </ul>
                    </div>
                    <!-- .top-links end -->
                </div>
            </div>
        </div> <!-- #top-bar end -->

        <!-- Header -->
        <header id="header" class="fix_header_bg">
            <div id="header-wrap">
                <div id="primary-menu-trigger">
                    <i class="icon-reorder"></i>
                </div>
                <div id="logo" class="mobile-device">
                    <a href="index.html" class="header_logo"><img src="{{ asset('') }}media/images/logo.png"
                            alt="Device Repair Script" width="" height=""></a>
                </div>
                <!-- #logo end -->
                <div class="container clearfix">
                    <div class="row header_full_width">
                        <div id="top-header-logo" class="col-12 col-md-12 col-lg-3 col-xl-3">
                            <!-- Logo -->
                            <div id="logo" class="desktop-device">
                                <a href="index.html" class="header_logo"
                                    data-dark-logo="https://www.repairdevice-demo2.moonwebsolutions.com/media/images/logo.png"><img
                                        src="{{ asset('') }}media/images/logo.png" alt="Device Repair Script"
                                        width="" height=""></a>
                                <a href="index.html" class="sticky_logo"
                                    data-dark-logo="https://www.repairdevice-demo2.moonwebsolutions.com/media/images/logo_fixed.png"><img
                                        src="{{ asset('') }}media/images/logo.png" alt="Device Repair Script"
                                        width="" height=""></a>
                            </div>
                            <!-- #logo end -->
                            <div class="right-s">
                                <div id="primary-menu-trigger">
                                    <i class="icon-times"></i>
                                </div>
                            </div>
                        </div>
                        <div id="top-search-col"
                            class="col-3 col-md-12 col-lg-9 col-xl-9 main_menu_v_center_section pr-0">
                            <!-- Primary Navigation-->
                            <nav id="primary-menu">
                                <ul class="main_menu_section menu-container">
                                    <li class=" ">
                                        <!--added by himansu on 22nd May 2021-->
                                        <a class="menu-link " href="#">
                                            <div>Services</div>
                                        </a>
                                        <!--end by himansu on 22nd May 2021-->
                                    </li>
                                    {{-- <li class="  current">
                                        <!--added by himansu on 22nd May 2021-->
                                        <a class="menu-link " href="#">
                                            <div>How it works</div>
                                        </a>
                                        <!--end by himansu on 22nd May 2021-->
                                        <ul class="sub-menu">
                                            <li class="current">
                                                <a href="#" class="">How Does Timoz works</a>
                                            </li>
                                        </ul>
                                    </li> --}}
                                    <li class=" ">
                                        <!--added by himansu on 22nd May 2021-->
                                        <a class="menu-link " href="#">
                                            <div>Why Us</div>
                                        </a>
                                        <!--end by himansu on 22nd May 2021-->
                                    </li>
                                    <li class=" ">
                                        <!--added by himansu on 22nd May 2021-->
                                        <a class="menu-link " href="#contact_us_section">
                                            <div>Contact</div>
                                        </a>
                                        <!--end by himansu on 22nd May 2021-->
                                    </li>
                                    <li class=" ">
                                        <!--added by himansu on 22nd May 2021-->
                                        <a class="menu-link " href="#">
                                            <div>Blog</div>
                                        </a>
                                        <!--end by himansu on 22nd May 2021-->
                                    </li>

                                </ul>
                                <!-- Top Search -->
                                <div id="top-search">
                                    <a href="#" id="top-search-trigger"><i class="icon-search3"></i><i
                                            class="icon-line-cross"></i></a>
                                    <form action="https://www.repairdevice-demo2.moonwebsolutions.com/search"
                                        method="get">
                                        <input type="text" name="search" id="srch_list_of_model"
                                            class="form-control srch_list_of_model" value="" placeholder="Search">
                                    </form>
                                </div>
                                <!-- #top-search end -->
                            </nav>
                            <!-- #primary-menu end -->
                        </div>
                    </div>
                </div>
            </div>
        </header> <!-- #header end -->

        <!--<link rel="stylesheet" href="https://www.repairdevice-demo2.moonwebsolutions.com/css/home-testimonials.css" type="text/css" />-->
        <section class="explore_repair_service new-explore">
            <div id="page-menu" class="">
                <div id="page-menu-wrap">
                    <div class="container-fluid">
                        <div class="page-menu-row">
                            <!-- <div class="page-menu-title">
  <span>Repair Services</span>
  </div> -->
                            <nav class="page-menu-nav one-page-menu">
                                <ul class="page-menu-container ers_section">
                                    <li class="page-menu-item"><i class="icon-check-circle"></i>30 minute service</li>
                                    <li class="page-menu-item"><i class="icon-check-circle"></i>90 days warranty</li>
                                    <li class="page-menu-item"><i class="icon-check-circle"></i>Book your appointment
                                        online</li>
                                    <li class="page-menu-item"><i class="icon-check-circle"></i>Service Provider</li>
                                    <li class="page-menu-item"><i class="icon-check-circle"></i>Original parts</li>
                                    <li class="page-menu-item"><i class="icon-check-circle"></i>No cure, no pay</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        @yield('main_content')







        <script src="{{ asset('') }}assets/js/jquery.js"></script>
        <script src='../www.google.com/recaptcha/api6b7f.js?onload=CaptchaCallback&amp;render=explicit'></script>
        <script src='../www.google.com/recaptcha/apic5fa.js?onload=CaptchaHomeNewsletterCallback&amp;render=explicit'></script>
        <script src="../unpkg.com/sweetalert%402.1.2/dist/sweetalert.min.js"></script>
        <script>
            var online_booking_hide_price = '0';
            var currency_symbol = "$";
            var disp_currency = "prefix";
            var is_space_between_currency_symbol = "0";
            var thousand_separator = ",";
            var decimal_separator = ".";
            var decimal_number = "1";
            var show_repair_item_price = '1';

            function format_amount(amount) {
                var symbol_space = "";
                if (is_space_between_currency_symbol == "1") {
                    symbol_space = " "
                } else {
                    symbol_space = ""
                }
                if (disp_currency == "prefix") {
                    return currency_symbol + symbol_space + amount;
                } else {
                    return amount + symbol_space + currency_symbol;
                }
            }

            function formatMoney(n, c, d, t) {
                var c = isNaN(c = Math.abs(c)) ? decimal_number : c,
                    d = d == undefined ? decimal_separator : d,
                    t = t == undefined ? thousand_separator : t,
                    s = n < 0 ? "-" : "",
                    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
                    j = (j = i.length) > 3 ? j % 3 : 0;
                return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(
                    n - i).toFixed(c).slice(2) : "");
            };

            function get_formatted_date(date) {
                var _date = new Date(date);
                return DateFormatter.format(_date, "M/d/Y");
            }

            function capitalizeFirstLetter(str) {
                return str.replace(/\w\S*/g, function(txt) {
                    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                });
            }
        </script>
        <script type="text/javascript">
            var popup_once_per_session = 1;

            function get_popup_cookie(Name) {
                var search = Name + "="
                var returnvalue = "";
                if (document.cookie.length > 0) {
                    offset = document.cookie.indexOf(search)
                    if (offset != -1) { // if cookie exists
                        offset += search.length
                        // set index of beginning of value
                        end = document.cookie.indexOf(";", offset);
                        // set index of end of cookie value
                        if (end == -1)
                            end = document.cookie.length;
                        returnvalue = unescape(document.cookie.substring(offset, end))
                    }
                }
                return returnvalue;
            }

            function offer_popup_showORnot() {
                var delay_time = 2000;
                if ((get_popup_cookie('promotion_offer_popup') == '') && (!popup_readCookie('promotion_offer_popup'))) {
                    setTimeout(function() {
                        jQuery('#instant_offer_popup').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }, delay_time);
                }
            }

            function popup_createCookie(name, value, days) {
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                    var expires = "; expires=" + date.toGMTString();
                } else {
                    var expires = "";
                }
                document.cookie = name + "=" + value + expires + "; path=/";
            }

            function popup_readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                }
                return null;
            }
            document.addEventListener("DOMContentLoaded", function() {
                if (popup_once_per_session == 1) {
                    offer_popup_showORnot();
                }
                jQuery('.close_offer_popup, .close_offer_popup_later').click(function() {
                    jQuery('#instant_offer_popup').modal('hide');
                    popup_createCookie('promotion_offer_popup', true, 1);
                    return false;
                });
                jQuery('.close_offer_popup_from_link_btn').click(function() {
                    jQuery('#instant_offer_popup').modal('hide');
                    popup_createCookie('promotion_offer_popup', true, 1);
                    return true;
                });
                jQuery('.close_offer_popup_no_thanks').click(function() {
                    jQuery('#instant_offer_popup').modal('hide');
                    popup_createCookie('promotion_offer_popup', true, 365);
                    return false;
                });
            });
        </script>
        <script src="{{ asset('') }}assets/js/plugins.js"></script>
        <!-- Footer Scripts
     ============================================= -->
        <script src="{{ asset('') }}assets/js/functions.js"></script>
        <!-- SLIDER REVOLUTION 5.x SCRIPTS  -->
        <!-- DatePicker JS -->
        <script src="{{ asset('') }}assets/js/components/datepicker.js"></script>
        <script src="{{ asset('') }}assets/js/jquery.autocomplete.min.js"></script>
        <script src="{{ asset('') }}assets/js/intlTelInput.js"></script>
        <script src="{{ asset('') }}assets/js/bootstrapvalidator.min.js"></script>
        <script src="{{ asset('') }}assets/js/slick.min.js"></script>
        <script src="{{ asset('') }}assets/js/jquery.matchHeight-min.js"></script>
        <script src="{{ asset('') }}assets/js/jquery.blockUI.js"></script>
        <script>
            jQuery(document).ready(function($) {
                $('.cookie-accept').click(function() {
                    $('#cookie-notification').animate({
                        bottom: -60
                    }, 400);
                    return false;
                });
                /***bottom footer***/
                // function footerAlign() {
                // $('footer').css('height', 'auto');
                // var footerHeight = $('footer').outerHeight();
                // $('body').css('padding-bottom', footerHeight);
                // $('footer').css('height', footerHeight);
                // }
                // jQuery(document).ready(function(){
                //   footerAlign();
                // });
                // jQuery( window ).resize(function() {
                //   footerAlign();
                // });
                /***end bottomfooter***/
            });
        </script>
        <script>
            var tpj = jQuery;
            tpj.noConflict();
            tpj(document).ready(function() {
                tpj('.home-slide').on('init', function() {
                    tpj('.home-slide').css('visibility', 'visible');
                });
                tpj('.home-slide').slick({
                    autoplay: true,
                    autoplaySpeed: 30000,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: true,
                    dots: true,
                    centerMode: true,
                });
                tpj('.slider-nav').slick({
                    autoplay: false,
                    slidesToShow: 3,
                    slidesToScroll: 1,

                    dots: true,

                    //dots: true,
                    focusOnSelect: true,
                    arrows: false,
                    responsive: [{
                            breakpoint: 3000,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3
                            }
                        },
                        {
                            breakpoint: 1920,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3
                            }
                        },
                        {
                            breakpoint: 1340,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3
                            }
                        },
                        {
                            breakpoint: 1025,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2
                            }
                        },
                        {
                            breakpoint: 767,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        },
                    ]
                });


                tpj('.srch_list_of_model').autocomplete({
                    serviceUrl: '/ajax/get_autocomplete_data.php?cat_id=' + 0 + '&brand_id=' + 0 +
                        '&series_id=' + 0,
                    onSelect: function(suggestion) {
                        window.location.href = suggestion.url;
                    },
                    onHint: function(hint) {
                        console.log("onHint");
                    },
                    onInvalidateSelection: function() {
                        console.log("onInvalidateSelection");
                    },
                    onSearchStart: function(params) {
                        console.log("onSearchStart");
                    },
                    onHide: function(container) {
                        console.log("onHide");
                    },
                    onSearchComplete: function(query, suggestions) {
                        console.log("onSearchComplete", suggestions);
                    },
                    //showNoSuggestionNotice: true,
                    //noSuggestionNotice: "We didn't find any matching devices...",
                });
                tpj('.srch_list_of_model').keydown(function(event) {
                    //if(event.keyCode == 13 && $(this).val() == "") {
                    if (event.keyCode == 13) {
                        return false;
                    }
                });
            });
            //ready
            tpj(document).ready(function($) {
                $('#footer_signup_form').bootstrapValidator({
                    fields: {
                        email: {
                            validators: {
                                notEmpty: {
                                    message: 'Please enter valid email address'
                                },
                                emailAddress: {
                                    message: 'Please enter valid email address'
                                }
                            }
                        }
                    }
                }).on('success.form.bv', function(e) {
                    $('#footer_signup_form').data('bootstrapValidator').resetForm();
                    // Prevent form submission
                    e.preventDefault();
                    // Get the form instance
                    var $form = $(e.target);
                    // Get the BootstrapValidator instance
                    var bv = $form.data('bootstrapValidator');
                    // Use Ajax to submit form data
                    $.post($form.attr('action'), $form.serialize(), function(result) {
                        console.log(result);
                    }, 'json');
                });
                //START for Newsletter form
                $('#instant_repair_cost_form').on('submit', function(e) {
                    e.preventDefault();
                    var full_name = $('#full_name').val();
                    var mob_number = $('#mob_number').val();
                    var mobile_brand = $('#mobile_brand').val();
                    var mobile_model = $('#mobile_model').val();
                    var mobile_query = $('#mobile_query').val();
                    if (full_name == '' || full_name == null) {
                        toastr.error('Full Name Is Required');
                        return false;
                    }
                    if (mob_number == '' || mob_number == null) {
                        toastr.error('Mobile No. Is Required');
                        return false;
                    }
                    // console.log(mob_number.length);
                    if (mob_number.length != 10) {
                        toastr.error('Please Enter Valid Mobile Number');
                        return false;
                    }
                    if (mobile_brand == '' || mobile_brand == null) {
                        toastr.error('Mobile Brand Name Is Required');
                        return false;
                    }
                    if (mobile_model == '' || mobile_model == null) {
                        toastr.error('Mobile Model Name Is Required');
                        return false;
                    }
                    if (mobile_query == '' || mobile_query == null) {
                        toastr.error("Please Enter Mobile's Issues");
                        return false;
                    }
                    var post_data = $('#instant_repair_cost_form').serialize();
                    // console.log(post_data); return false;
                    $.ajax({
                        type: "POST",
                        url: "{{ route('new-order-form-save-ajax') }}",
                        data: new FormData(this),
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function(data) {
                            if (data.status == 'success') {
                                toastr.success(data.message);
                                setTimeout(function() {

                                    location.reload(true);
                                }, 3000);

                            }
                            if (data.status == 'error') {
                                toastr.error(data.message);
                                setTimeout(function() {

                                    location.reload(true);
                                }, 3000);
                            }


                        }
                    });
                });
                $('.btn-close, .close_newsletter_popup').click(function() {
                    $('#NewsLetter').modal('hide');
                    $('.newsletter_fields').show();
                    $('.resp_newsletter').hide();
                    $(".newsletter_fields input[type=text]").val('');
                    $(".newsletter_fields input[type=email]").val('');
                });
                //END for Newsletter form
                //START for check booking available
                var disabled_date_arr = ["2021-11-11", "2021-11-26", "2021-12-09", "2021-12-22", "2021-12-14",
                    "2021-12-30", "2021-12-28"
                ];
                $('.repair_appt_date').datepicker({
                    autoclose: true,
                    minDate: 0,
                    startDate: "today",
                    todayHighlight: true,
                    daysOfWeekDisabled: [0, 6],
                    beforeShowDay: function(date) {
                        var dd = (date.getDate() < 10 ? '0' : '') + date.getDate();
                        var MM = ((date.getMonth() + 1) < 10 ? '0' : '') + (date.getMonth() + 1);
                        var yyyy = date.getFullYear();
                        //var dmy = date.getFullYear() + "-" + ((date.getMonth() + 1) < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1)) + "-" + date.getDate();
                        var dmy = yyyy + '-' + MM + '-' + dd;
                        //console.log('dmy',dmy,'disabled_date_arr',disabled_date_arr);
                        if (disabled_date_arr.indexOf(dmy) != -1) {
                            return false;
                        }
                    },
                    format: 'dd/mm/yyyy' //added by himansu on 21st Dec 2021 for UK fix format
                }).on('changeDate', function(e) {
                    getTimeSlotListByDate();
                });
                $('.repair_time_slot').on('change', function(e) {
                    check_booking_available();
                });
                //END for check booking available
                $('.datepicker').datepicker({
                    autoclose: true
                });
                // setTimeout(function () {
                $('.portfolio-item .inner, .testimonial, .clients-grid li a').matchHeight();
                // }, 4000);
                /*var loading = 'data:image/gif;base64,R0lGODlhIAAgAPMAAP///wAAAMbGxoSEhLa2tpqamjY2NlZWVtjY2OTk5Ly8vB4eHgQEBAAAAAAAAAAAACH+GkNyZWF0ZWQgd2l0aCBhamF4bG9hZC5pbmZvACH5BAAKAAAAIf8LTkVUU0NBUEUyLjADAQAAACwAAAAAIAAgAAAE5xDISWlhperN52JLhSSdRgwVo1ICQZRUsiwHpTJT4iowNS8vyW2icCF6k8HMMBkCEDskxTBDAZwuAkkqIfxIQyhBQBFvAQSDITM5VDW6XNE4KagNh6Bgwe60smQUB3d4Rz1ZBApnFASDd0hihh12BkE9kjAJVlycXIg7CQIFA6SlnJ87paqbSKiKoqusnbMdmDC2tXQlkUhziYtyWTxIfy6BE8WJt5YJvpJivxNaGmLHT0VnOgSYf0dZXS7APdpB309RnHOG5gDqXGLDaC457D1zZ/V/nmOM82XiHRLYKhKP1oZmADdEAAAh+QQACgABACwAAAAAIAAgAAAE6hDISWlZpOrNp1lGNRSdRpDUolIGw5RUYhhHukqFu8DsrEyqnWThGvAmhVlteBvojpTDDBUEIFwMFBRAmBkSgOrBFZogCASwBDEY/CZSg7GSE0gSCjQBMVG023xWBhklAnoEdhQEfyNqMIcKjhRsjEdnezB+A4k8gTwJhFuiW4dokXiloUepBAp5qaKpp6+Ho7aWW54wl7obvEe0kRuoplCGepwSx2jJvqHEmGt6whJpGpfJCHmOoNHKaHx61WiSR92E4lbFoq+B6QDtuetcaBPnW6+O7wDHpIiK9SaVK5GgV543tzjgGcghAgAh+QQACgACACwAAAAAIAAgAAAE7hDISSkxpOrN5zFHNWRdhSiVoVLHspRUMoyUakyEe8PTPCATW9A14E0UvuAKMNAZKYUZCiBMuBakSQKG8G2FzUWox2AUtAQFcBKlVQoLgQReZhQlCIJesQXI5B0CBnUMOxMCenoCfTCEWBsJColTMANldx15BGs8B5wlCZ9Po6OJkwmRpnqkqnuSrayqfKmqpLajoiW5HJq7FL1Gr2mMMcKUMIiJgIemy7xZtJsTmsM4xHiKv5KMCXqfyUCJEonXPN2rAOIAmsfB3uPoAK++G+w48edZPK+M6hLJpQg484enXIdQFSS1u6UhksENEQAAIfkEAAoAAwAsAAAAACAAIAAABOcQyEmpGKLqzWcZRVUQnZYg1aBSh2GUVEIQ2aQOE+G+cD4ntpWkZQj1JIiZIogDFFyHI0UxQwFugMSOFIPJftfVAEoZLBbcLEFhlQiqGp1Vd140AUklUN3eCA51C1EWMzMCezCBBmkxVIVHBWd3HHl9JQOIJSdSnJ0TDKChCwUJjoWMPaGqDKannasMo6WnM562R5YluZRwur0wpgqZE7NKUm+FNRPIhjBJxKZteWuIBMN4zRMIVIhffcgojwCF117i4nlLnY5ztRLsnOk+aV+oJY7V7m76PdkS4trKcdg0Zc0tTcKkRAAAIfkEAAoABAAsAAAAACAAIAAABO4QyEkpKqjqzScpRaVkXZWQEximw1BSCUEIlDohrft6cpKCk5xid5MNJTaAIkekKGQkWyKHkvhKsR7ARmitkAYDYRIbUQRQjWBwJRzChi9CRlBcY1UN4g0/VNB0AlcvcAYHRyZPdEQFYV8ccwR5HWxEJ02YmRMLnJ1xCYp0Y5idpQuhopmmC2KgojKasUQDk5BNAwwMOh2RtRq5uQuPZKGIJQIGwAwGf6I0JXMpC8C7kXWDBINFMxS4DKMAWVWAGYsAdNqW5uaRxkSKJOZKaU3tPOBZ4DuK2LATgJhkPJMgTwKCdFjyPHEnKxFCDhEAACH5BAAKAAUALAAAAAAgACAAAATzEMhJaVKp6s2nIkolIJ2WkBShpkVRWqqQrhLSEu9MZJKK9y1ZrqYK9WiClmvoUaF8gIQSNeF1Er4MNFn4SRSDARWroAIETg1iVwuHjYB1kYc1mwruwXKC9gmsJXliGxc+XiUCby9ydh1sOSdMkpMTBpaXBzsfhoc5l58Gm5yToAaZhaOUqjkDgCWNHAULCwOLaTmzswadEqggQwgHuQsHIoZCHQMMQgQGubVEcxOPFAcMDAYUA85eWARmfSRQCdcMe0zeP1AAygwLlJtPNAAL19DARdPzBOWSm1brJBi45soRAWQAAkrQIykShQ9wVhHCwCQCACH5BAAKAAYALAAAAAAgACAAAATrEMhJaVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqUToLA7VxF0JDyIQh/MVVPMt1ECZlfcjZJ9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyASyNDJ0uIiRMDjI0Fd30/iI2UA5GSS5UDj2l6NoqgOgN4gksEBgYFf0FDqKgHnyZ9OX8HrgYHdHpcHQULXAS2qKpENRg7eAMLC7kTBaixUYFkKAzWAAnLC7FLVxLWDBLKCwaKTULgEwbLA4hJtOkSBNqITT3xEgfLpBtzE/jiuL04RGEBgwWhShRgQExHBAAh+QQACgAHACwAAAAAIAAgAAAE7xDISWlSqerNpyJKhWRdlSAVoVLCWk6JKlAqAavhO9UkUHsqlE6CwO1cRdCQ8iEIfzFVTzLdRAmZX3I2SfZiCqGk5dTESJeaOAlClzsJsqwiJwiqnFrb2nS9kmIcgEsjQydLiIlHehhpejaIjzh9eomSjZR+ipslWIRLAgMDOR2DOqKogTB9pCUJBagDBXR6XB0EBkIIsaRsGGMMAxoDBgYHTKJiUYEGDAzHC9EACcUGkIgFzgwZ0QsSBcXHiQvOwgDdEwfFs0sDzt4S6BK4xYjkDOzn0unFeBzOBijIm1Dgmg5YFQwsCMjp1oJ8LyIAACH5BAAKAAgALAAAAAAgACAAAATwEMhJaVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqUToLA7VxF0JDyIQh/MVVPMt1ECZlfcjZJ9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyASyNDJ0uIiUd6GGl6NoiPOH16iZKNlH6KmyWFOggHhEEvAwwMA0N9GBsEC6amhnVcEwavDAazGwIDaH1ipaYLBUTCGgQDA8NdHz0FpqgTBwsLqAbWAAnIA4FWKdMLGdYGEgraigbT0OITBcg5QwPT4xLrROZL6AuQAPUS7bxLpoWidY0JtxLHKhwwMJBTHgPKdEQAACH5BAAKAAkALAAAAAAgACAAAATrEMhJaVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqUToLA7VxF0JDyIQh/MVVPMt1ECZlfcjZJ9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyASyNDJ0uIiUd6GAULDJCRiXo1CpGXDJOUjY+Yip9DhToJA4RBLwMLCwVDfRgbBAaqqoZ1XBMHswsHtxtFaH1iqaoGNgAIxRpbFAgfPQSqpbgGBqUD1wBXeCYp1AYZ19JJOYgH1KwA4UBvQwXUBxPqVD9L3sbp2BNk2xvvFPJd+MFCN6HAAIKgNggY0KtEBAAh+QQACgAKACwAAAAAIAAgAAAE6BDISWlSqerNpyJKhWRdlSAVoVLCWk6JKlAqAavhO9UkUHsqlE6CwO1cRdCQ8iEIfzFVTzLdRAmZX3I2SfYIDMaAFdTESJeaEDAIMxYFqrOUaNW4E4ObYcCXaiBVEgULe0NJaxxtYksjh2NLkZISgDgJhHthkpU4mW6blRiYmZOlh4JWkDqILwUGBnE6TYEbCgevr0N1gH4At7gHiRpFaLNrrq8HNgAJA70AWxQIH1+vsYMDAzZQPC9VCNkDWUhGkuE5PxJNwiUK4UfLzOlD4WvzAHaoG9nxPi5d+jYUqfAhhykOFwJWiAAAIfkEAAoACwAsAAAAACAAIAAABPAQyElpUqnqzaciSoVkXVUMFaFSwlpOCcMYlErAavhOMnNLNo8KsZsMZItJEIDIFSkLGQoQTNhIsFehRww2CQLKF0tYGKYSg+ygsZIuNqJksKgbfgIGepNo2cIUB3V1B3IvNiBYNQaDSTtfhhx0CwVPI0UJe0+bm4g5VgcGoqOcnjmjqDSdnhgEoamcsZuXO1aWQy8KAwOAuTYYGwi7w5h+Kr0SJ8MFihpNbx+4Erq7BYBuzsdiH1jCAzoSfl0rVirNbRXlBBlLX+BP0XJLAPGzTkAuAOqb0WT5AH7OcdCm5B8TgRwSRKIHQtaLCwg1RAAAOwAAAAAAAAAAADxiciAvPgo8Yj5XYXJuaW5nPC9iPjogIG15c3FsX3F1ZXJ5KCkgWzxhIGhyZWY9J2Z1bmN0aW9uLm15c3FsLXF1ZXJ5Jz5mdW5jdGlvbi5teXNxbC1xdWVyeTwvYT5dOiBDYW4ndCBjb25uZWN0IHRvIGxvY2FsIE15U1FMIHNlcnZlciB0aHJvdWdoIHNvY2tldCAnL3Zhci9ydW4vbXlzcWxkL215c3FsZC5zb2NrJyAoMikgaW4gPGI+L2hvbWUvYWpheGxvYWQvd3d3L2xpYnJhaXJpZXMvY2xhc3MubXlzcWwucGhwPC9iPiBvbiBsaW5lIDxiPjY4PC9iPjxiciAvPgo8YnIgLz4KPGI+V2FybmluZzwvYj46ICBteXNxbF9xdWVyeSgpIFs8YSBocmVmPSdmdW5jdGlvbi5teXNxbC1xdWVyeSc+ZnVuY3Rpb24ubXlzcWwtcXVlcnk8L2E+XTogQSBsaW5rIHRvIHRoZSBzZXJ2ZXIgY291bGQgbm90IGJlIGVzdGFibGlzaGVkIGluIDxiPi9ob21lL2FqYXhsb2FkL3d3dy9saWJyYWlyaWVzL2NsYXNzLm15c3FsLnBocDwvYj4gb24gbGluZSA8Yj42ODwvYj48YnIgLz4KPGJyIC8+CjxiPldhcm5pbmc8L2I+OiAgbXlzcWxfcXVlcnkoKSBbPGEgaHJlZj0nZnVuY3Rpb24ubXlzcWwtcXVlcnknPmZ1bmN0aW9uLm15c3FsLXF1ZXJ5PC9hPl06IENhbid0IGNvbm5lY3QgdG8gbG9jYWwgTXlTUUwgc2VydmVyIHRocm91Z2ggc29ja2V0ICcvdmFyL3J1bi9teXNxbGQvbXlzcWxkLnNvY2snICgyKSBpbiA8Yj4vaG9tZS9hamF4bG9hZC93d3cvbGlicmFpcmllcy9jbGFzcy5teXNxbC5waHA8L2I+IG9uIGxpbmUgPGI+Njg8L2I+PGJyIC8+CjxiciAvPgo8Yj5XYXJuaW5nPC9iPjogIG15c3FsX3F1ZXJ5KCkgWzxhIGhyZWY9J2Z1bmN0aW9uLm15c3FsLXF1ZXJ5Jz5mdW5jdGlvbi5teXNxbC1xdWVyeTwvYT5dOiBBIGxpbmsgdG8gdGhlIHNlcnZlciBjb3VsZCBub3QgYmUgZXN0YWJsaXNoZWQgaW4gPGI+L2hvbWUvYWpheGxvYWQvd3d3L2xpYnJhaXJpZXMvY2xhc3MubXlzcWwucGhwPC9iPiBvbiBsaW5lIDxiPjY4PC9iPjxiciAvPgo8YnIgLz4KPGI+V2FybmluZzwvYj46ICBteXNxbF9xdWVyeSgpIFs8YSBocmVmPSdmdW5jdGlvbi5teXNxbC1xdWVyeSc+ZnVuY3Rpb24ubXlzcWwtcXVlcnk8L2E+XTogQ2FuJ3QgY29ubmVjdCB0byBsb2NhbCBNeVNRTCBzZXJ2ZXIgdGhyb3VnaCBzb2NrZXQgJy92YXIvcnVuL215c3FsZC9teXNxbGQuc29jaycgKDIpIGluIDxiPi9ob21lL2FqYXhsb2FkL3d3dy9saWJyYWlyaWVzL2NsYXNzLm15c3FsLnBocDwvYj4gb24gbGluZSA8Yj42ODwvYj48YnIgLz4KPGJyIC8+CjxiPldhcm5pbmc8L2I+OiAgbXlzcWxfcXVlcnkoKSBbPGEgaHJlZj0nZnVuY3Rpb24ubXlzcWwtcXVlcnknPmZ1bmN0aW9uLm15c3FsLXF1ZXJ5PC9hPl06IEEgbGluayB0byB0aGUgc2VydmVyIGNvdWxkIG5vdCBiZSBlc3RhYmxpc2hlZCBpbiA8Yj4vaG9tZS9hamF4bG9hZC93d3cvbGlicmFpcmllcy9jbGFzcy5teXNxbC5waHA8L2I+IG9uIGxpbmUgPGI+Njg8L2I+PGJyIC8+Cg==';
                $('.lazy').Lazy({
                delay:2000,
                effect: 'show',
                effectTime: 1000,
                visibleOnly: true,
                defaultImage: loading,
                });*/
            });

            function block_div(div_id) {
                // Block Element
                tpj("#" + div_id).block({
                    message: '<i class="icon-spinner icon-spin"></i>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        cursor: 'wait',
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            }

            function unblock_div(div_id) {
                tpj("#" + div_id).unblock();
            }
            /***parallax***/
            window.onmousemove = function(e) {
                let moveNum = (e.pageX / innerWidth) * 180;
                let moveNum2 = (e.pageY / innerHeight) * 180;
                let newNum = ((e.pageX - innerWidth / 2) / (innerWidth / 2));
                let newNum2 = ((e.pageY - innerHeight / 2) / (innerHeight / 2));
                //console.log(newNum)
                //console.log(document.querySelector(".parallax"));
                if (document.querySelector(".parallax")) {
                    document.querySelector(".parallax").style.transform =
                        `translate(${-0 - (newNum*10)}%,${-0 - (newNum2*10)}%)`
                }
            }
            jQuery('#footer_newsletter').bootstrapValidator({
                fields: {
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Please enter valid email address'
                            },
                            emailAddress: {
                                message: 'Please enter valid email address'
                            }
                        }
                    }
                }
            });

            function check_newsletter_footer_captcha() {
                if (jQuery('#nl_f_g_captcha_token').val() == "") {
                    alert("Invalid captcha");
                    return false;
                } else {
                    return true;
                }
            }
        </script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" type="text/javascript"></script>

        <script>
            jQuery(document).ready(function($) {
                jQuery.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            });
        </script>
        @yield('script_content')

</body>

<!-- Mirrored from www.repairdevice-demo2.moonwebsolutions.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 17 Apr 2022 09:41:54 GMT -->

</html>
