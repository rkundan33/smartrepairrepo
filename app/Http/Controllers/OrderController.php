<?php

namespace App\Http\Controllers;

use App\CustomerOrder;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function newOrderFormSaveAjax(Request $request)
    {
       try {
           $order = new CustomerOrder();
           $order->customer_name = $request->full_name;
        //    $order->email = $request->email;
           $order->mobile = $request->mob_number;
           $order->phone_brand = $request->mobile_brand;
           $order->phone_model = $request->mobile_model;
           $order->phone_issue = $request->mobile_query;
           $order->save();
           return response()->json(['status' => 'success','message' => 'Thanks for your request ! Our agent will call you soon !']);
        } catch (\Throwable $th) {
           return response()->json(['status' => 'error','message' => $th->getMessage()]);
           //throw $th;
       }
    }
}
