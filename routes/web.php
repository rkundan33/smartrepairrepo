<?php

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
     // Here you have the data that you needW

    // dd('aaa',$data['phone_models_arr']);
    return view('index');

});
Route::post('/new-order-form-save-ajax', 'OrderController@newOrderFormSaveAjax')->name('new-order-form-save-ajax');
