<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerOrder extends Model
{
    protected $fillable = ['id','customer_name','email',
                          'mobile','phone_brand','phone_model','phone_issue','is_resolved','created_at','updated_at'];
}
