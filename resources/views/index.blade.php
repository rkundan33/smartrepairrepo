@extends('layouts.app')
@section('style_content')
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" type="text/css" />
    {{-- <link rel="stylesheet" href="{{ asset('') }}assets/css/selectize.css"> --}}
@endsection
@section('main_content')
    <section id="showcase" class="slider_bg_color sl-main-slider py-0" style="background-color:#FAFAFA;">
        <!-- <div class="area">
                       <div></div>
                       <div></div>
                       <div></div>
                       <div></div>
                       <div></div>
                       <div></div>
                    </div> -->
        <div class="home-slide">
            <div class="home-slide-item">
                <div class="banner"
                    style="background:url('media/images/section/20211210091506.png') no-repeat 0 0;background-size:cover">
                    <div class="container">
                        <div class="home-slide-inner">
                            <div class="left">
                                <div class="block showcase-text calculate-cost pr-0 mr-0">
                                    <h1>Most Trusted & Safe Call - Out Mobile Phone<br> Repair Services</h1>
                                    <p>No Hassle as We come to you and fix your problems</p>
                                </div>
                            </div>
                            <div class="right">
                                <div class="block px-0 mx-0 pb-0 mb-0 showcase-image">
                                    <img class="animated fadeIn vert-move"
                                        src="{{ asset('') }}media/images/section/120211210091804.png"
                                        alt="Most Trusted & Safe Call - Out Mobile Phone<br> Repair Services">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="home-slide-item">
                <div class="banner"
                    style="background:url('media/images/section/20211210091506.png') no-repeat 0 0;background-size:cover">
                    <div class="container">
                        <div class="home-slide-inner">
                            <div class="left">
                                <div class="block showcase-text calculate-cost pr-0 mr-0">
                                    <h1>Repairs - Whenever, Wherever You Need Us!</h1>
                                    <p>Book Your Repair In 1 Minute Or Less.</p>

                                </div>
                            </div>
                            <div class="right">
                                <div class="block px-0 mx-0 pb-0 mb-0 showcase-image">
                                    <img class="animated fadeIn vert-move"
                                        src="{{ asset('') }}media/images/section/220211210091804.png"
                                        alt="Repairs - Whenever, Wherever You Need Us!">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- <section class="brand_section_category" style="background-color:#FAFAFA; ">
        <!-- border-bottom -->
        <div class="section brand nobg m-0 py-5">
            <div class="container clearfix center">
                <div class="clear"></div>
                <div class="heading-block bottommargin-sm center">
                    <h3>Select your device <strong>type</strong></h3>
                    <form class="form-inline" action="https://www.repairdevice-demo2.moonwebsolutions.com/search"
                        method="post">
                        <div class="form-group">
                            <input type="text" name="search"
                                class="form-control border-bottom border-top-0 border-right-0 border-left-0 mx-auto srch_list_of_model"
                                id="autocomplete" placeholder="Search for your device here..." autocomplete="off"
                                tabindex="-1">
                        </div>
                    </form>
                </div>
                <div class=" nobottommargin clearfix ">
                    <div class="row center-grid">
                        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 device_category">
                            <a href="product-category/phone.html" class="card align-items-center">
                                <div class="img inner mx-auto">
                                    <img class="main_img"
                                        src="{{ asset('') }}media/images/categories/20210105055025.png" alt="Phone">
                                    <h4>Phone</h4>
                                </div>
                            </a>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 device_category">
                            <a href="product-category/laptop.html" class="card align-items-center">
                                <div class="img inner mx-auto">
                                    <img class="main_img"
                                        src="{{ asset('') }}media/images/categories/20210105055123.png" alt="Laptop">
                                    <h4>Laptop</h4>
                                </div>
                            </a>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 device_category">
                            <a href="product-category/smartwatch.html" class="card align-items-center">
                                <div class="img inner mx-auto">
                                    <img class="main_img"
                                        src="{{ asset('') }}media/images/categories/20210105055142.png"
                                        alt="Smartwatch">
                                    <h4>Smartwatch</h4>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}
    <section class="pb-5 why_choose_us" style="background-color:#FFFFFF; ">
        <a name="why-choose-us"></a>
        <div class="container clearfix">
            <div class="clear"></div>
            <div class="heading-block pt-5 pb-4 mb-0 center">
                <h3 style="color:#000000" !important>Why Choose <strong>Us</strong></h3>
            </div>
            <div class="why-choose pb-5">
                <div class="row">
                    <div class="col-md-4 fbox-section ">
                        <div class="feature-box fbox-center fbox-light fbox-effect">
                            <div class="fbox-icon">
                                <img class="lazy"
                                    src="{{ asset('') }}media/images/section/220210623121325.png">
                            </div>
                            <h3>QUICK & EASY PROCESS</h3>
                            <div class="services-title-border"></div>
                            <p>Any of the booking methods on offer are quick and easy! In most cases you can complete your
                                repair booking in a minute or less!</p>
                        </div>
                    </div>
                    <div class="col-md-4 fbox-section ">
                        <div class="feature-box fbox-center fbox-light fbox-effect">
                            <div class="fbox-icon">
                                <img class="lazy"
                                    src="{{ asset('') }}media/images/section/320210623121049.png">
                            </div>
                            <h3>HIGH WORK STANDARD</h3>
                            <div class="services-title-border"></div>
                            <p>We don't own your device, but we fix it as if we did! You can count on us for a high quality
                                finish when it comes to your device repair.</p>
                        </div>
                    </div>
                    <div class="col-md-4 fbox-section  col_last">
                        <div class="feature-box fbox-center fbox-light fbox-effect">
                            <div class="fbox-icon">
                                <img class="lazy"
                                    src="{{ asset('') }}media/images/section/120210623121147.png">
                            </div>
                            <h3>HUNDREDS OF REVIEWS</h3>
                            <div class="services-title-border"></div>
                            <p>Don't just take our word for it, there are hundreds if not thousands of reviews across the
                                internet.</p>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <section id="get_instant_repair_cost"
        style="background-color:#EAEAEA; background:url('media/images/section/20210623064024.png') no-repeat 0 0;background-size: cover;">
        <div class="container clearfix">
            <a name="request_quote"></a>
            <div class="clear"></div>
            <div class="heading-block pt-4 mb-0 center">

                <h3 style="color:#FFFFFF" !important>Register Repair <strong>Now</strong></h3>
            </div>
            <form class="mb-0 py-4" method="post" id="instant_repair_cost_form">
                <div class="form-row center-grid">
                    <div class="form-group col-md-3">
                        <input type="text" name="full_name" id="full_name" class="form-control" placeholder="Full Name">
                    </div>
                    <div class="form-group col-md-3">
                        <!-- <label style="color:#FFFFFF" !important>Select Brand</label> -->
                        <input type="number"  name="mob_number" id="mob_number" class="form-control"
                            placeholder="Mob. No.">
                    </div>
                    <div class="col-md-3 form-group">
                        <input type="text" name="mobile_brand" id="mobile_brand" class="form-control"
                            placeholder="Enter Mobile Brand Name">
                    </div>
                    <div class="col-md-3 form-group">
                        <input type="text" name="mobile_model" id="mobile_model" class="form-control"
                            placeholder="Enter Mobile Model No.">

                    </div>
                    <div class="col-6 form-group" id="mobile_query_col">
                        <textarea name="mobile_query" id="mobile_query" class="form-control"
                            placeholder="Enter Your Problems Here"></textarea>

                    </div>
                </div>
                <div class="col-md-12 center">
                    <button type="submit" class="button rounded-pill" name="submit_quote" id="submit_quote">Submit</button>
                </div>
                <input type="hidden" name="csrf_token"
                    value="d86710fece0af96d1dc7ff80da865bc4a292685556cdeb779dbc336e83dfb3cb">
            </form>
        </div>
    </section>
    <section class="py-4 easy_step" style="background-color:#F1F1F1; " id="section-about">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block how-to-work text-center">
                        <div class="heading-block topmargin-lg bottommargin-sm center">
                            <h3>Easy as 1, 2, <strong>3</strong></h3>
                        </div>
                        <div class="row center_content">
                            <div class="col-12 col-md-6 col-lg-4 col-xl-4">
                                <div class="feature-box fbox-center fbox-light fbox-effect">
                                    <div class="fbox-icon"><img
                                            src="{{ asset('') }}media/images/section/120210610134425.png"
                                            class="img-fluid" alt=""></div>
                                    <h3>Choose Your Repair</h3>
                                    <p>We can repair your broken device, If you are unsure of whats wrong, our trained
                                        technicians can conduct a diagnostic test!</p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 col-xl-4">
                                <div class="feature-box fbox-center fbox-light fbox-effect">
                                    <div class="fbox-icon"><img
                                            src="{{ asset('') }}media/images/section/220210610134425.png"
                                            class="img-fluid" alt=""></div>
                                    <h3>Choose Your Repair Method</h3>
                                    <p>Bring your device to store, request a home or workplace call out or simply opt for
                                        our nationwide mail order repair service</p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 col-xl-4">
                                <div class="feature-box fbox-center fbox-light fbox-effect">
                                    <div class="fbox-icon"><img
                                            src="{{ asset('') }}media/images/section/320210610134425.png"
                                            class="img-fluid" alt=""></div>
                                    <h3>Receive Your Working Device</h3>
                                    <p>Whichever option you choose, we aim to have your device repaired and dispatched on
                                        the same working day as we receive.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="get_review_section py-5" style="background-color:#EA16E6; ">
        <div style="background-size:cover;" class="mt-0 nobottommargin nobottompadding nobottomborder" style="">
            <div class="clearfix">
                <div class="clear"></div>
                <div class="heading-block center my-1">
                    <h3>REAL REVIEWS FROM REAL <strong>CUSTOMERS</strong></h3>
                </div>

                <div class="row">
                    <div class="col-md-12">

                        <div class="card p-3 bg-light mt-4">
                            <h3>Google Reviews</h3>

                            <span class="wp-stars"><span class="wp-star"><svg width="17" height="17"
                                        viewBox="0 0 1792 1792">
                                        <path
                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                            fill="#e7711b"></path>
                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                        viewBox="0 0 1792 1792">
                                        <path
                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                            fill="#e7711b"></path>
                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                        viewBox="0 0 1792 1792">
                                        <path
                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                            fill="#e7711b"></path>
                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                        viewBox="0 0 1792 1792">
                                        <path
                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                            fill="#e7711b"></path>
                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                        viewBox="0 0 1792 1792">
                                        <path
                                            d="M1250 957l257-250-356-52-66-10-30-60-159-322v963l59 31 318 168-60-355-12-66zm452-262l-363 354 86 500q5 33-6 51.5t-34 18.5q-17 0-40-12l-449-236-449 236q-23 12-40 12-23 0-34-18.5t-6-51.5l86-500-364-354q-32-32-23-59.5t54-34.5l502-73 225-455q20-41 49-41 28 0 49 41l225 455 502 73q45 7 54 34.5t-24 59.5z"
                                            fill="#e7711b"></path>
                                    </svg></span></span>
                            <br>
                            <span>Based on 24 reviews</span>

                            <ul class="testimonials-grid review_grid_section grid-2 clearfix">

                                <li>
                                    <div class="testimonial">
                                        <div class="testi-image">
                                            <a href="https://www.google.com/maps/contrib/101892546811434973527/place/ChIJJ82C2gmNcUgRKdBPgGXrfyE"
                                                target="_blank"><img
                                                    src="https://lh3.googleusercontent.com/a-/AOh14GjRJXeZ8SJaJTF5JVe7t08xSCBxeKUn5W2zKHfe770=s128-c0x00000000-cc-rp-mo-ba6"></a>
                                        </div>
                                        <div class="testi-content">
                                            <p>Run by universities who know what they are doing, I have only made enquiries
                                                but get a good feel already, and I have seen their good reviews and plan to
                                                use.</p>
                                            <span class="wp-stars"><span class="wp-star"><svg width="17"
                                                        height="17" viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                                        viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                                        viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                                        viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                                        viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span></span>

                                            <div class="testi-meta">
                                                Jacek Goronski <span>a month ago</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="testimonial">
                                        <div class="testi-image">
                                            <a href="https://www.google.com/maps/contrib/108671200312026628976/place/ChIJJ82C2gmNcUgRKdBPgGXrfyE"
                                                target="_blank"><img
                                                    src="https://lh3.googleusercontent.com/a/AATXAJy-If6ZhxDniSEYiycKUo0MrILKj2Kd5ntNopbQ=s128-c0x00000000-cc-rp-mo"></a>
                                        </div>
                                        <div class="testi-content">
                                            <p>I warmly recommend the excellent service provided by FixMyCrack: Ishaan
                                                magnificently gave my old ailing laptop a sparkling new lease of life. It's
                                                now so fast & responsive!! Ishaan is kind, friendly, knowledgeable, explains
                                                well, gives helpful advice, and kept me updated throughout the repair
                                                process, and charged a wonderfully fair and affordable price. I strongly
                                                recommend FixMyCrack :-)</p>
                                            <span class="wp-stars"><span class="wp-star"><svg width="17"
                                                        height="17" viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                                        viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                                        viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                                        viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                                        viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span></span>

                                            <div class="testi-meta">
                                                Barbara Tomlin <span>a month ago</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="testimonial">
                                        <div class="testi-image">
                                            <a href="https://www.google.com/maps/contrib/100187710309537561853/place/ChIJJ82C2gmNcUgRKdBPgGXrfyE"
                                                target="_blank"><img
                                                    src="https://lh3.googleusercontent.com/a/AATXAJyJ3_ZG4SEFF_2t7XWkQlhoE-Y-ghmmuMsLLiD5=s128-c0x00000000-cc-rp-mo"></a>
                                        </div>
                                        <div class="testi-content">
                                            <p>Gave their blood sweat and tears to clean my phone! Fast and friendly, would
                                                recommend!</p>
                                            <span class="wp-stars"><span class="wp-star"><svg width="17"
                                                        height="17" viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                                        viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                                        viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                                        viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                                        viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span></span>

                                            <div class="testi-meta">
                                                Patrick Shortall <span>2 weeks ago</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="testimonial">
                                        <div class="testi-image">
                                            <a href="https://www.google.com/maps/contrib/109799828038889021440/place/ChIJJ82C2gmNcUgRKdBPgGXrfyE"
                                                target="_blank"><img
                                                    src="https://lh3.googleusercontent.com/a-/AOh14GgkY36f2ahGOsLTK6Gr_TTUbGOSjGh-tqpYbIxwww=s128-c0x00000000-cc-rp-mo"></a>
                                        </div>
                                        <div class="testi-content">
                                            <p>Super friendly and flexible. I dropped off my phone just before my lecture
                                                and it was ready once I was finished. Very well priced as well, a students
                                                dream. Cheers team 🙌</p>
                                            <span class="wp-stars"><span class="wp-star"><svg width="17"
                                                        height="17" viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                                        viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                                        viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                                        viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                                        viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span></span>

                                            <div class="testi-meta">
                                                Frankie Woolf <span>5 months ago</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="testimonial">
                                        <div class="testi-image">
                                            <a href="https://www.google.com/maps/contrib/115633609583065108741/place/ChIJJ82C2gmNcUgRKdBPgGXrfyE"
                                                target="_blank"><img
                                                    src="https://lh3.googleusercontent.com/a/AATXAJwwl928yLq9fe9YMaPrNpsNG5_xhJdxpPCF1_C31g=s128-c0x00000000-cc-rp-mo"></a>
                                        </div>
                                        <div class="testi-content">
                                            <p>cheap, quality service with the added convenience of meeting on campus</p>
                                            <span class="wp-stars"><span class="wp-star"><svg width="17"
                                                        height="17" viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                                        viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                                        viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                                        viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span><span class="wp-star"><svg width="17" height="17"
                                                        viewBox="0 0 1792 1792">
                                                        <path
                                                            d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                                                            fill="#e7711b"></path>
                                                    </svg></span></span>

                                            <div class="testi-meta">
                                                I Flashman <span>a month ago</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                            </ul>

                            {{-- B/610, Empire Busine --}}

                        </div>

                    </div>
                </div>

                <!--start new slider-->
                <div class="row slider-nav">
                    <div class="oc-item">
                        <div class="testimonial">
                            <div class="testi-image">
                                <img class="" src="{{ asset('') }}media/images/placeholder_avatar.jpg"
                                    alt="Review Avatar">
                            </div>
                            <div class="testi-content">
                                <p>Lalit is one of best and professional guy meet in 10 years of business.I haven’t... <a
                                        href="index.html" data-toggle="modal" data-target="#reviewModal53">Read More</a></p>
                                <div class="testi-meta">
                                    Mattia Guadagno <span>
                                    </span>
                                </div>
                                <div class="testi-stars"><i class="icon-star3"></i><i
                                        class="icon-star3"></i><i class="icon-star3"></i><i
                                        class="icon-star3"></i><i class="icon-star3"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="oc-item">
                        <div class="testimonial">
                            <div class="testi-image">
                                <img class="" src="{{ asset('') }}media/images/placeholder_avatar.jpg"
                                    alt="Review Avatar">
                            </div>
                            <div class="testi-content">
                                <p>mr lalit is so good and so much experience which is very important to me... <a
                                        href="index.html" data-toggle="modal" data-target="#reviewModal52">Read More</a></p>
                                <div class="testi-meta">
                                    Phone Store Uden <span>
                                    </span>
                                </div>
                                <div class="testi-stars"><i class="icon-star3"></i><i
                                        class="icon-star3"></i><i class="icon-star3"></i><i
                                        class="icon-star3"></i><i class="icon-star3"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="oc-item">
                        <div class="testimonial">
                            <div class="testi-image">
                                <img class="" src="{{ asset('') }}media/images/placeholder_avatar.jpg"
                                    alt="Review Avatar">
                            </div>
                            <div class="testi-content">
                                <p>Without a doubt they deliver. Outstanding website speed. Always willing to go the extra
                                    mile.... <a href="index.html" data-toggle="modal" data-target="#reviewModal51">Read
                                        More</a></p>
                                <div class="testi-meta">
                                    Freddy <span>
                                    </span>
                                </div>
                                <div class="testi-stars"><i class="icon-star3"></i><i
                                        class="icon-star3"></i><i class="icon-star3"></i><i
                                        class="icon-star3"></i><i class="icon-star3"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="oc-item">
                        <div class="testimonial">
                            <div class="testi-image">
                                <img class="" src="{{ asset('') }}media/images/placeholder_avatar.jpg"
                                    alt="Review Avatar">
                            </div>
                            <div class="testi-content">
                                <p>Have dealt with Business Choice for a few years. Happy with the overall service
                                    however... <a href="index.html" data-toggle="modal" data-target="#reviewModal50">Read
                                        More</a></p>
                                <div class="testi-meta">
                                    Ian - Master Mobile <span>
                                    </span>
                                </div>
                                <div class="testi-stars"><i class="icon-star3"></i><i
                                        class="icon-star3"></i><i class="icon-star3"></i><i
                                        class="icon-star3"></i><i class="icon-star3"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end new slider-->
                <div class="modal fade" id="reviewModal53" tabindex="-1" role="dialog"
                    aria-labelledby="reviewModalLabel53" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="reviewModalLabel53">Mattia Guadagno </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>
                                <div style="text-align: justify;"><span
                                        style="color: rgb(50, 50, 61); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 16px; letter-spacing: 0.5px;">Lalit
                                        is one of best and professional guy meet in 10 years of business.</span></div><span
                                    style="color: rgb(50, 50, 61); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 16px; letter-spacing: 0.5px;">
                                    <div style="text-align: justify;">I haven’t other words...it’s simply</div>
                                </span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="reviewModal52" tabindex="-1" role="dialog"
                    aria-labelledby="reviewModalLabel52" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="reviewModalLabel52">Phone Store Uden </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p style="text-align: justify; "><span
                                        style="color: rgb(50, 50, 61); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 16px; letter-spacing: 0.5px;">mr
                                        lalit is so good and so much experience which is very important to me he is super
                                        fast in answering your questions and solving your problems. really recommended.
                                        thank you mr lalit for your patience and time</span><br></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="reviewModal51" tabindex="-1" role="dialog"
                    aria-labelledby="reviewModalLabel51" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="reviewModalLabel51">Freddy </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p style="text-align: justify; "><span
                                        style="color: rgb(50, 50, 61); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 16px; letter-spacing: 0.5px;">Without
                                        a doubt they deliver. Outstanding website speed. Always willing to go the extra
                                        mile. We hired them to design our new corporate website. We were able to describe
                                        our visual design through email and some samples and they were able to bring it to
                                        life. If you're looking for a website designer outside of North America make sure
                                        you go with this company. The team fixed any issues extremely fast without fuss.
                                        Their bankend of website is easy to use too! 10/10</span><br></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="reviewModal50" tabindex="-1" role="dialog"
                    aria-labelledby="reviewModalLabel50" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="reviewModalLabel50">Ian - Master Mobile </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>
                                <div style="text-align: justify;"><span
                                        style="color: rgb(50, 50, 61); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 16px; letter-spacing: 0.5px;">Have
                                        dealt with Business Choice for a few years. Happy with the overall service however
                                        must point out that both Anton and Chloe went above and beyond, surprisingly even
                                        though i thought i may have to cancel.</span></div><span
                                    style="color: rgb(50, 50, 61); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 16px; letter-spacing: 0.5px;">
                                    <div style="text-align: justify;">They both deserve a pay rise!</div>
                                </span>
                                <div style="text-align: justify;">
                                    <font color="#32323d" face="Helvetica Neue, Helvetica, Arial, sans-serif"><span
                                            style="font-size: 16px; letter-spacing: 0.5px;"><br></span></font>
                                </div><span
                                    style="color: rgb(50, 50, 61); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 16px; letter-spacing: 0.5px;">
                                    <div style="text-align: justify;">Thanks again both.</div>
                                </span></p>
                            </div>
                        </div>
                    </div>
                </div>
    </section>
    <section class="fill-free-question nobottommargin nobottomborder skrollable skrollable-between py-5"
        style="background-color:#B7EA49; ">
        <div class="container clearfix">
            <div class="clear"></div>
            <div class="row">
                <div class="col-lg-4 text-center">
                    <div class="get_quote_address">
                        <!-- <img src="https://www.repairdevice-demo2.moonwebsolutions.com/media/images/logo.png" alt="Device Repair Script" style="display:block;" class="bottommargin-sm bottom-logo" width="" height=""> -->
                        {{-- <div class="quote_address">
                            <span>Address </span>
                            <p>B/610, Empire Business Hub, Science City Road, Sola, Ahmedabad, Gujarat, 380060</p>
                        </div> --}}
                        <div class="quote_wrk_hrs">
                            <span>Working Hours</span>
                            <p> 9 AM - 10 PM</p>
                        </div>
                        <div class="quote_call_us">
                            <span>Call us</span>
                            <p><a class="border-0" href="tel:6201018042">6201018042</a></p>
                        </div>
                        <div class="social_link"><a
                                class="social-icon si-small si-borderless si-colored si-rounded si-facebook"
                                href="https://www.facebook.com/" target="_blank" title="https://www.facebook.com"><i
                                    class="icon-facebook"></i><i class="icon-facebook"></i></a><a
                                class="social-icon si-small si-borderless si-colored si-rounded si-twitter"
                                href="https://twitter.com/" target="_blank" title="https://twitter.com"><i
                                    class="icon-twitter"></i><i class="icon-twitter"></i></a><a
                                class="social-icon si-small si-borderless si-colored si-rounded si-linkedin"
                                href="https://www.linkedin.com/" target="_blank" title="https://www.linkedin.com"><i
                                    class="icon-linkedin"></i><i class="icon-linkedin"></i></a><a
                                class="social-icon si-small si-borderless si-colored si-rounded si-pinterest"
                                href="https://www.pinterest.com/" target="_blank" title="https://www.pinterest.com/"><i
                                    class="icon-pinterest"></i><i class="icon-pinterest"></i></a><a
                                class="social-icon si-small si-borderless si-colored si-rounded si-youtube"
                                href="https://www.youtube.com/" target="_blank" title="https://www.youtube.com"><i
                                    class="icon-youtube"></i><i class="icon-youtube"></i></a><a
                                class="social-icon si-small si-borderless si-colored si-rounded si-instagram"
                                href="https://www.instagram.com/" target="_blank" title="https://www.instagram.com"><i
                                    class="icon-instagram"></i><i class="icon-instagram"></i></a></div>
                    </div>
                </div>
                <div class="col-lg-8" id="contact_us_section">
                    <div class="heading-block topmargin-lg bottommargin-sm">
                        <h3 style="color:#000000" !important>QUESTIONS? FEEL FREE TO CONTACT <strong>US.</strong></h3>
                    </div>
                    <form action="https://www.repairdevice-demo2.moonwebsolutions.com/controllers/contact.php" method="post"
                        id="contact_form" class="mb-0">
                        <div class="form-row">
                            <div class="col-md-4">
                                <label for="name">Name <small>*</small></label>
                                <input type="text" name="name" id="name" value="" class="form-control required" />
                            </div>
                            <div class="col-md-4">
                                <label for="email">Email <small>*</small></label>
                                <input type="text" name="email" id="email" value="" class="form-control required" />
                            </div>
                            <div class="col-md-4">
                                <label for="cell_phone">Phone <small>*</small></label>
                                <input type="tel" id="cell_phone" name="cell_phone" class="form-control required" value="">
                                <input type="hidden" name="phone" id="phone" />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 topmargin-sm">
                                <label for="message">Message <small>*</small></label>
                                <textarea name="message" id="message" class="required form-control" rows="6" cols="30"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 topmargin-sm">
                                <button class="button button-3d nomargin rounded-pill" type="submit"
                                    id="template-contactform-submit" name="template-contactform-submit"
                                    value="submit">Submit</button>
                                <input type="hidden" name="submit_form" id="submit_form" />
                            </div>
                        </div>
                        <input type="hidden" name="mode" id="mode" value="home_page" />
                        <input type="hidden" name="csrf_token"
                            value="996c44961c5d3f7a502328923e1dc474703957b6dd9e3a7980e040bee50ded65">
                    </form>
                </div>
            </div>
        </div>
    </section>
    {{-- <section class="py-4 location-section" style="background-color:#4A93ED; " id="section-about">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block how-to-work text-center">
                        <div class="heading-block topmargin-lg bottommargin-sm center">
                            <h3 style="color:#000000" !important><strong>Locations</strong></h3><span
                                class="divcenter" style="color:#000000" !important>We have multiple locations to surve
                                you</span>
                        </div>
                        <div class="row center_content location-section-inner">
                            <div class="col-12 col-md-6 col-lg-6 col-xl-6">
                                <div class="feature-box fbox-center fbox-light fbox-effect">
                                    <a href="location-1.html">
                                        <h3>Ahmedabad Science city area</h3>
                                        <div class="location-i"><img
                                                src="{{ asset('') }}media/images/store_location/20210224120141.jpg"
                                                alt="location" /></div><span class="location-address-section">Unit 22,
                                            Burntwood Town Shopping Centre<br> Chase Terrace, Sankeys Corner, WS7 1JR</span>
                                    </a><br><a target="_blank" class="btn"
                                        href="https://maps.google.it/maps?q=Unit+22,Burntwood+Town+Shopping+Centre+Chase+Terrace+Sankeys+Corner+UK+WS7+1JR">
                                        Get Directions</a>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-6 col-xl-6">
                                <div class="feature-box fbox-center fbox-light fbox-effect">
                                    <a href="location-2.html">
                                        <h3>Ahmedabad Paldi</h3>
                                        <div class="location-i"><img
                                                src="{{ asset('') }}media/images/store_location/20210506132638.jpg"
                                                alt="location" /></div><span class="location-address-section">19 De La
                                            Beche Road<br> Mumbai, Sketty, SA2 9AR</span>
                                    </a><br><a target="_blank" class="btn"
                                        href="https://maps.google.it/maps?q=19+De+La+Beche+Road+Mumbai+Sketty+United+Kingdom+SA2+9AR">
                                        Get Directions</a><br>
                                    <div class="location-i">Call us</div><a href="tel:12345678"><span
                                            class="mobile-number">12345678</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="nobottomborder center py-4 notopmargin">
            <a href="branches.html" class="button rounded-pill">Locations Details <i
                    class="icon-circle-arrow-right"></i></a>
        </div>
    </section> --}}
    {{-- <section style="background-color:#20F54F; ">
        <div class="section brand nobg m-0 pt-3 pb-4">
            <div class="container clearfix center">
                <div class="clear"></div>
                <div class="heading-block topmargin-sm bottommargin-sm center">
                    <h3>We Fix Them <strong>All</strong></h3>
                </div>
                <ul class="clients-grid grid-5 nobottommargin clearfix center-grid device-brands">
                    <li>
                        <a href="lenovo.html" class="d-flex align-items-center">
                            <div class="arrow"><i class="icon-check"></i></div>
                            <div class="inner mx-auto">
                                <img class="lazy" src="{{ asset('') }}media/images/brand/20190903072124.png"
                                    alt="lenovo">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="apple.html" class="d-flex align-items-center">
                            <div class="arrow"><i class="icon-check"></i></div>
                            <div class="inner mx-auto">
                                <img class="lazy" src="{{ asset('') }}media/images/brand/20210304140414.png"
                                    alt="Apple">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="google.html" class="d-flex align-items-center">
                            <div class="arrow"><i class="icon-check"></i></div>
                            <div class="inner mx-auto">
                                <img class="lazy" src="{{ asset('') }}media/images/brand/20210304140833.png"
                                    alt="Google">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="samsung.html" class="d-flex align-items-center">
                            <div class="arrow"><i class="icon-check"></i></div>
                            <div class="inner mx-auto">
                                <img class="lazy" src="{{ asset('') }}media/images/brand/20210304140545.png"
                                    alt="Samsung">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="sony.html" class="d-flex align-items-center">
                            <div class="arrow"><i class="icon-check"></i></div>
                            <div class="inner mx-auto">
                                <img class="lazy" src="{{ asset('') }}media/images/brand/20190903072107.png"
                                    alt="SONY">
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section> --}}
    <section class="faults_repaire" style="background-color:#FFFFFF; ">
        <div class="container clearfix">
            <div class="clear"></div>
            <div class="heading-block topmargin-lg bottommargin-sm center fr_title_section">
                <h3>REPAIR <strong>SERVICES</strong></h3>
            </div>
            <div class="row service-slider-nav">
                <div class="col-12 col-md-4 col-lg-3 p-3">
                    <div class="feature-box fbox-center fbox-light fbox-plain fbox-services fr_inner_section">
                        <div class="fbox-icon">
                            <img class="lazy" src="{{ asset('') }}media/images/service/20201103113721.png"
                                alt="Tablet & iPad" />
                        </div>
                        <h3>Tablet & iPad</h3>
                        <p>Competently accurate data after equity invested architectures.</p>
                    </div>
                </div>
                <div class="col-12 col-md-4 col-lg-3 p-3">
                    <div class="feature-box fbox-center fbox-light fbox-plain fbox-services fr_inner_section">
                        <div class="fbox-icon">
                            <img class="lazy" src="{{ asset('') }}media/images/service/20201103113901.png"
                                alt="Charging Port" />
                        </div>
                        <h3>Charging Port</h3>
                        <p>Competently accurate data after equity invested architectures.</p>
                    </div>
                </div>
                <div class="col-12 col-md-4 col-lg-3 p-3">
                    <div class="feature-box fbox-center fbox-light fbox-plain fbox-services fr_inner_section">
                        <div class="fbox-icon">
                            <img class="lazy" src="{{ asset('') }}media/images/service/20201103114022.png"
                                alt="ISight Camera" />
                        </div>
                        <h3>ISight Camera</h3>
                        <p>Competently accurate data after equity invested architectures.</p>
                    </div>
                </div>
                <div class="col-12 col-md-4 col-lg-3 p-3">
                    <div class="feature-box fbox-center fbox-light fbox-plain fbox-services fr_inner_section">
                        <div class="fbox-icon">
                            <img class="lazy" src="{{ asset('') }}media/images/service/20201103114157.png"
                                alt="Side Button" />
                        </div>
                        <h3>Side Button</h3>
                        <p>Competently accurate data after equity invested architectures.</p>
                    </div>
                </div>
                <div class="col-12 col-md-4 col-lg-3 p-3">
                    <div class="feature-box fbox-center fbox-light fbox-plain fbox-services fr_inner_section">
                        <div class="fbox-icon">
                            <img class="lazy" src="{{ asset('') }}media/images/service/20201103114403.png"
                                alt="Power Button" />
                        </div>
                        <h3>Power Button</h3>
                        <p>Competently accurate data after equity invested architectures.</p>
                    </div>
                </div>
                <div class="col-12 col-md-4 col-lg-3 p-3">
                    <div class="feature-box fbox-center fbox-light fbox-plain fbox-services fr_inner_section">
                        <div class="fbox-icon">
                            <img class="lazy" src="{{ asset('') }}media/images/service/20201103114529.png"
                                alt="Battery" />
                        </div>
                        <h3>Battery</h3>
                        <p>Competently accurate data after equity invested architectures.</p>
                    </div>
                </div>
                <div class="col-12 col-md-4 col-lg-3 p-3">
                    <div class="feature-box fbox-center fbox-light fbox-plain fbox-services fr_inner_section">
                        <div class="fbox-icon">
                            <img class="lazy" src="{{ asset('') }}media/images/service/20201103120718.png"
                                alt="Software/Unlocking" />
                        </div>
                        <h3>Software/Unlocking</h3>
                        <p>Competently accurate data after equity invested architectures.</p>
                    </div>
                </div>
                <div class="col-12 col-md-4 col-lg-3 p-3">
                    <div class="feature-box fbox-center fbox-light fbox-plain fbox-services fr_inner_section">
                        <div class="fbox-icon">
                            <img class="lazy" src="{{ asset('') }}media/images/service/20201103120804.png"
                                alt="Screen" />
                        </div>
                        <h3>Screen</h3>
                        <p>Competently accurate data after equity invested architectures.</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            {{-- <div class="heading-block nobottomborder center my-2">
                <a href="services.html" class="button rounded-pill">View All Services <i
                        class="icon-circle-arrow-right"></i></a>
            </div> --}}
        </div>
    </section>

    <!-- <section class="main_faq_section padding-60 py-5" style="background-color:#FFFFFF; " id="faq">
        <div class="container clearfix">
        <div class="clear"></div>
        <div class="heading-block topmargin-lg bottommargin-sm center fr_title_section">
          <h3 >Any Questions or <strong>Concerns?</strong></h3><span class="divcenter" >Faq Sub title</span></div>
        <div class="row">
          <div class="col-md-12">
              <div class="col_full nobottommargin"><div class="accordion accordion-border faq_section clearfix" data-state="closed"><div class="acctitle"><i class="icon-plus"></i><i class="icon-minus"></i>How long will i be without my device?</div><div class="acc_content clearfix"><p>In-Store Appointments - In-store appointment repair times can be completed in as little as 15 minutes however this is subject to stock availability and the complexity of the repair service in question. As an overall promise, we always advise that any repair does not go over 5 business days. </p><p>We Come To You - We arrive in one of our specially designed vehicles to complete the repair service at your home or place of work, If a repair is booked using this method, the repair should take no longer than 60 minutes. </p><p>Mail Order - From the time of booking, we get to work. All of the relevant parts are ordered and reserved. This means that we are all set and ready to go by the time your device arrives with us at Repair HQ. We boast that 90% of repairs booked via our Mail Order Repair Service are received, repaired, tested, and dispatched back to you, the customer on the same working day. This meaning that from posting to receipt your repaired device is back in your hand in as little as 72 hours. </p></div><div class="acctitle"><i class="icon-plus"></i><i class="icon-minus"></i>Will I be updated throughout the repair process?</div><div class="acc_content clearfix"><p style="text-align: center; "><font color="#555555" face="Lato, sans-serif"><span style="font-size: 16px;"><b>We have been repairing devices for many years - we understand that customer communication is key to being able to give a second to none service. We have a specially designed system in order to automatically update you every step of the way via email and sms. These are real time updates so that you are kept in the loop along side our customer account managers. </b></span></font><br></p></div><div class="acctitle"><i class="icon-plus"></i><i class="icon-minus"></i>Am I Able To Speak To Someone - Or Can I Only Book Online?</div><div class="acc_content clearfix"><p>In Short - YES! We have a full customer service team ready to help with your requests and are available between 6 AM - Midnight 7 Days A Week. </p><p>You are able to get up to date information on the latest pricing and service availability in your area. - Existing Customer? They can give you up to date information regarding the status of your repair process. </p></div></div></div>                      <div class="heading-block nobottomborder center my-2">
                      <a href="https://repairdevice-demo2.moonwebsolutions.com/faqs" class="button rounded-pill">View All Faq(s)kk <i class="icon-circle-arrow-right"></i></a>
                  </div>
                </div>
        </div>
        </div>
        </section> -->
    <section class="main_faq_section padding-60 py-5" style="background-color:#FFFFFF; " id="faq">
        <div class="container clearfix">
            <div class="clear"></div>
            <div class="heading-block topmargin-lg bottommargin-sm center fr_title_section">
                <h3>Any Questions or <strong>Concerns?</strong></h3><span class="divcenter">Faq Sub title</span>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="faq_page">
                        <div class="faq_header_section">
                            <div id="accordion-faqs" class="accordion">
                                <div class="block clearfix faq_question">
                                    <div class="card">
                                        <div class="card-header" id="heading1">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapse1" aria-expanded="true"
                                                aria-controls="collapse1"><strong>How long will i be without my
                                                    device?</strong><span><i class="icon-plus"></i><i
                                                        class="icon-minus"></i></span></button>
                                        </div>
                                        <div id="collapse1" class="collapse show-no" aria-labelledby="heading"
                                            data-parent="#accordion-faqs">
                                            <div class="card-body">
                                                <p>In-Store Appointments - In-store appointment repair times can be
                                                    completed in as little as 15 minutes however this is subject to stock
                                                    availability and the complexity of the repair service in question. As an
                                                    overall promise, we always advise that any repair does not go over 5
                                                    business days. </p>
                                                <p>We Come To You - We arrive in one of our specially designed vehicles to
                                                    complete the repair service at your home or place of work, If a repair
                                                    is booked using this method, the repair should take no longer than 60
                                                    minutes. </p>
                                                <p>Mail Order - From the time of booking, we get to work. All of the
                                                    relevant parts are ordered and reserved. This means that we are all set
                                                    and ready to go by the time your device arrives with us at Repair HQ. We
                                                    boast that 90% of repairs booked via our Mail Order Repair Service are
                                                    received, repaired, tested, and dispatched back to you, the customer on
                                                    the same working day. This meaning that from posting to receipt your
                                                    repaired device is back in your hand in as little as 72 hours. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="heading2">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapse2" aria-expanded="true"
                                                aria-controls="collapse2"><strong>Will I be updated throughout the repair
                                                    process?</strong><span><i class="icon-plus"></i><i
                                                        class="icon-minus"></i></span></button>
                                        </div>
                                        <div id="collapse2" class="collapse " aria-labelledby="heading"
                                            data-parent="#accordion-faqs">
                                            <div class="card-body">
                                                <p style="text-align: center; ">
                                                    <font color="#555555" face="Lato, sans-serif"><span
                                                            style="font-size: 16px;"><b>We have been repairing devices for
                                                                many years - we understand that customer communication is
                                                                key to being able to give a second to none service. We have
                                                                a specially designed system in order to automatically update
                                                                you every step of the way via email and sms. These are real
                                                                time updates so that you are kept in the loop along side our
                                                                customer account managers. </b></span></font><br>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="heading3">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapse3" aria-expanded="true"
                                                aria-controls="collapse3"><strong>Am I Able To Speak To Someone - Or Can I
                                                    Only Book Online?</strong><span><i class="icon-plus"></i><i
                                                        class="icon-minus"></i></span></button>
                                        </div>
                                        <div id="collapse3" class="collapse " aria-labelledby="heading"
                                            data-parent="#accordion-faqs">
                                            <div class="card-body">
                                                <p>In Short - YES! We have a full customer service team ready to help with
                                                    your requests and are available between 6 AM - Midnight 7 Days A Week.
                                                </p>
                                                <p>You are able to get up to date information on the latest pricing and
                                                    service availability in your area. - Existing Customer? They can give
                                                    you up to date information regarding the status of your repair process.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section style="background-color:#FFFFFF; ">
        <!-- <div class="container clearfix"> -->
        <div class="clear"></div>
        <div class="heading-block topmargin-lg bottommargin-sm center">
            <h3>custom <strong>made</strong></h3>
        </div>
        <!-- TrustBox script -->
        <script type="text/javascript" src="../widget.trustpilot.com/bootstrap/v5/tp.widget.sync.bootstrap.min.js" async="">
        </script>
        <!-- End Trustbox script -->

        <!-- TrustBox widget - Carousel -->
        <div class="trustpilot-widget" data-locale="nl-NL" data-template-id="53aa8912dec7e10d38f59f36"
            data-businessunit-id="5ee780838d23530001971b90" data-style-height="140px" data-style-width="100%"
            data-theme="light" data-stars="4,5" data-review-languages="nl">cxcxzczxczxczxczxczxc</div> <!-- </div> -->
    </section>
    <footer id="footer" class="main-footer-section">
        <div class="container">
            <!-- Footer Widget
                 ============================================= -->
            <div class="footer-widgets-wrap pb-3 clearfix">
                <div class="row clearfix">
                    <div class="col-lg-5">
                        <div class="widget clearfix">
                            <div class="widget notopmargin clearfix">
                                <div class="row clearfix">
                                    <div class="col-lg-12 bottommargin-sm clearfix">
                                        <img src="{{ asset('') }}media/images/logo.png" alt="Device Repair Script"
                                            style="display:block;" class="bottommargin-sm bottom-logo" width="" height="">
                                        <p>All product and company names are trademarks of their respective holders. iPhone,
                                            iPad, iPod, iPod touch, Mac, and iMac are registered trademarks and property of
                                            Apple, Inc. Fixpal is a third-party repair company and is not affiliated with
                                            Apple, Samsung, Google, LG, Huawei, or any other brand on this site<span
                                                style="color: rgb(255, 255, 255); background-color: rgb(255, 255, 0);"></span>
                                        </p>
                                        <!-- <form id="footer_signup_form" action="https://www.repairdevice-demo2.moonwebsolutions.com/signup" method="get" class="nobottommargin">
                                          <div class="input-group signup_button">
                                             <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="icon-email2"></i></div>
                                             </div>
                                             <input type="email" name="email" id="email" class="form-control required email" placeholder="Enter your Email" required>
                                             <div class="input-group-append">
                                                <button class="btn btn-info" type="submit">Submit</button>
                                             </div>
                                          </div>
                                       </form> -->
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 bottommargin-sm clearfix">
                                    <a class="social-icon si-small si-borderless si-colored si-rounded si-facebook"
                                        href="https://www.facebook.com/" target="_blank" title="https://www.facebook.com"><i
                                            class="icon-facebook"></i><i class="icon-facebook"></i></a><a
                                        class="social-icon si-small si-borderless si-colored si-rounded si-twitter"
                                        href="https://twitter.com/" target="_blank" title="https://twitter.com"><i
                                            class="icon-twitter"></i><i class="icon-twitter"></i></a><a
                                        class="social-icon si-small si-borderless si-colored si-rounded si-linkedin"
                                        href="https://www.linkedin.com/" target="_blank" title="https://www.linkedin.com"><i
                                            class="icon-linkedin"></i><i class="icon-linkedin"></i></a><a
                                        class="social-icon si-small si-borderless si-colored si-rounded si-pinterest"
                                        href="https://www.pinterest.com/" target="_blank"
                                        title="https://www.pinterest.com/"><i class="icon-pinterest"></i><i
                                            class="icon-pinterest"></i></a><a
                                        class="social-icon si-small si-borderless si-colored si-rounded si-youtube"
                                        href="https://www.youtube.com/" target="_blank" title="https://www.youtube.com"><i
                                            class="icon-youtube"></i><i class="icon-youtube"></i></a><a
                                        class="social-icon si-small si-borderless si-colored si-rounded si-instagram"
                                        href="https://www.instagram.com/" target="_blank"
                                        title="https://www.instagram.com"><i class="icon-instagram"></i><i
                                            class="icon-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="row clearfix">
                            <div class="col-md-4 col-lg-4 col-sm-4 col-6 bottommargin-sm">
                                <div class="widget widget_links app_landing_widget_link clearfix">
                                    <h4>PAGES</h4>
                                    <ul>
                                        <li>
                                            <a class="" href="#">Services</a>
                                        </li>
                                        <li>
                                            <a class="" href="#">Contact Us</a>
                                        </li>
                                        <li>
                                            <a class="" href="#">FAQ</a>
                                        </li>
                                        <li>
                                            <a class="" href="#">Reviews</a>
                                        </li>
                                        <li>
                                            <a class="" href="#">Blog</a>
                                        </li>
                                        <li>
                                            <a class="" href="#">lalit page</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-4 col-6 bottommargin-sm">
                                <div class="widget widget_links app_landing_widget_link clearfix">
                                    <h4>Repairs</h4>
                                    <ul>
                                        <li>
                                            <a class="" href="#">Repair</a>
                                        </li>
                                        <li>
                                            <a class="" href="#">Quick Repair</a>
                                        </li>
                                        <li>
                                            <a class="" href="#">Custom Quote</a>
                                        </li>
                                        <li>
                                            <a class="" href="#">Bulk Repair</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-4 bottommargin-sm">
                                <div class="widget widget_links app_landing_widget_link clearfix">
                                    <h4>CONTACT</h4>
                                    <span class="customer-support"><i class="icon-mobile-alt"></i></span>
                                    <div class="suppor_phone"><a class="border-0"
                                            href="tel:6201018042"><span>6201018042</span></a></div> <span
                                        class="customer-support"><i class="icon-line2-envelope"></i></span>
                                    <div class="support_email"><a
                                            href="mailto:customercare@smartrepairservice.store"><span>customercare@smartrepairservice.store</span></a>
                                    </div> <span class="customer-support"><i class="icon-map-marker1"></i></span>
                                    <div class="support_address">Uttamnagar East,Delhi-110059</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="copyrights" class="nobg border-top footer_bottom">
            <div class="container clearfix">
                <div class="row">
                    <div class="col-md-6">
                        Designed & Developed By <a href="https://www.instagram.com/rkundan27/" target="_blank">Kundan
                            Mandal</a> </div>
                    <div class="col-md-6">
                        <div class="copyrights-menu copyright-links clearfix text-right">
                            <a class="" href="#">Terms & Conditions</a> <span class='bottom_footer'>/</span>
                            <a class="" href="#">Privacy
                                Policy</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #copyrights end -->
        <!-- Copyrights ============================================= -->

        <div class="sticky_call">
            <div class="float-sm">
                <div class="fl-fl float-whatsapp">
                    <a href="tel:6201018042"><i class="icon-call" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>

    </footer>
    <div class="basel-close-side"></div>
    <!-- #footer end -->
    </div><!-- #wrapper end -->
    <!--cookie section--->
    <div id="cookie-notification" class="dark">
        <div class="container clearfix">
            <div class="row">
                <!-- <div class="col-xl- col-lg-4 col-md-3 col-1"></div> -->
                <div class="col-xl-12 col-lg-12 col-md-12 cookies-detail">
                    <h4>This Website uses cookies.</h4>
                    <h5>We inform you that this site uses own, technical cookies to make sure our web page<span tabindex=kk>
                    </h5>
                    <a href="javascript:void(0);" class="cookie-accept cookie-noti-btn btn btn-danger btn-sm">Accept</a>
                </div>
                <!-- <div class="col-xl-4 col-lg-4 col-md-3 col-1">  </div> -->
            </div>
        </div>
    </div>
    <!-- Go To Top
           ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>
    <!-- External JavaScripts
           ============================================= -->
    <div class="modal fade common_popup promotion_offer instance_offer" id="instant_offer_popup" tabindex="-1"
        role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content"
                style="background-color:#https://www.repairdevice-demo2.moonwebsolutions.com/how-it-works" !important>
                <button type="button" class="close close_offer_popup quick_close" data-dismiss="modal"
                    aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="modal-body p-0">
                    <div class="row ">
                        <div class="col-md-5">
                            <img class="close_instance_offer"
                                src="{{ asset('') }}media/images/offer_popup_image.jpg" />
                        </div>
                        <div class="col-md-7 instant_offer_detail">
                            <div class="modal-header">
                                <h3 class="modal-title">Repair Your Device</h3>
                            </div>
                            <div class="modal-body">
                                <p class="textt">Get Your Device Repaired At</p>
                                <p class="textt">Your Doorstep In Hygienic Way !</p>
                                <p style="margin-top: 9px !important">Get 3 Months Warrenty Too</p>
                            </div>
                            <div class="modal-footer footer_btn_section">
                                <a href="javascript:void(0)" class="close_offer_popup_later btn-off offer-button">ok Let's
                                    Go !</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script_content')
    {{-- <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script> --}}

    <script>
        var tpj = jQuery;
        tpj.noConflict();
        tpj(document).ready(function() {
            if (window.matchMedia("(max-width: 768px)").matches) {
                tpj('.service-slider-nav').slick({
                    //slidesToShow: 1,
                    focusOnSelect: true,
                    arrows: false,
                    dots: true,
                    autoplaySpeed: 5000,
                    autoplay: true,
                    slidesToScroll: 1,
                    slidesPerRow: 2,
                    rows: 2,
                    //dotsClass:'service-slick-dots',
                });
            }



        });
        //ready
    </script>
@endsection
